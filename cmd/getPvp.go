// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

// getPvpCmd represents the getPvp command
var getPvpCmd = &cobra.Command{
	Use:   "getPvp",
	Short: "Get your PvP stats, games or standings ",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		conf := getConfig()
		//apikey := cmd.Flag("apikey").Value.String()
		class := cmd.Flag("class").Value.String()
		id := cmd.Flag("id").Value.String()
		expected := []string{"stats", "games","standings"}
		//expected_args := []string{"sells", "buys"}
		//if apikey == "" {
		//	fmt.Println("please provide an apikey")
		//	os.Exit(0)
		//}
		if Contains(expected,args[0]) {
			var pvpStats Pvp
			if args[0] == "stats" {
				errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/pvp/%s?access_token=%s",args[0],conf.Apikey), &pvpStats)
				if errJson != nil{
					fmt.Println(errJson)
				}
				if class != "" {
					fmt.Println(pvpStats.Professions.getField(class).Format())
				}
			}else if args[0] == "games"{
				if id == "" {
					var games []string
					errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/pvp/%s?access_token=%s",args[0],conf.Apikey), &games)
					if errJson != nil{
						fmt.Println(errJson)
					}
					fmt.Println(games)
				}else{
					var game Game
					errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/pvp/%s/%s?access_token=%s",args[0],id,conf.Apikey), &game)
					if errJson != nil{
						fmt.Println(errJson)
					}
					fmt.Println(game.Format())
				}
			}else if args[0] == "standings"{
				var games []Season
				errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/pvp/%s?access_token=%s",args[0],conf.Apikey), &games)
				if errJson != nil{
					fmt.Println(errJson)
				}
				fmt.Println("Division: Current/Best etc...")
				for i:=0;i<len(games) ;i++  {
					fmt.Println(games[i].Format())
				}
			}
		}
	},
}

func init() {
	getPvpCmd.PersistentFlags().String("apikey", "", "Provide apikey with PvP rights")
	getPvpCmd.PersistentFlags().String("class", "", "Provide class you want stats for")
	getPvpCmd.PersistentFlags().String("id", "", "Provide game ID")
	rootCmd.AddCommand(getPvpCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getPvpCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getPvpCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
