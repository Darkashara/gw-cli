// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"io/ioutil"
	"strconv"
)

// extractCmd represents the extract command
var extractCmd = &cobra.Command{
	Use:   "extract",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		conf := getConfig()
		runes := cmd.Flag("runes").Value.String()
		sigils := cmd.Flag("sigils").Value.String()
		minProfit := cmd.Flag("min").Value.String()
		DoSetItemFiles(conf)
		sigilsValues, runesValues := getItemExtract(conf)
		var ecto ItemPrice
		url := fmt.Sprintf("https://api.guildwars2.com/v2/commerce/prices?ids=%s&lang=%s", 19721, conf.Lang)
		getJson(url,&ecto)
		if sigils == "false" && runes == "false"{
			fmt.Println("PROCESSING SIGILS...")
			//ids := getUpgradesComponent(sigilsValues)
			//var item []ItemPrice
			//url := fmt.Sprintf("https://api.guildwars2.com/v2/commerce/prices?ids=%s&lang=%s", ids, conf.Lang)
			//errJson := getJson(url,&item)
			//if errJson != nil {
			//	fmt.Println(errJson)
			//}
			//for s:=0; s<len(item); s++ {
			//	for w:=0; w<len(sigilsValues[item[s].Id]); w++{
			//		profit := float64(item[s].Buys.UnitPrice+ecto.Sells.UnitPrice)*0.899
			//		profit = profit*0.85
			//		profit -= float64(sigilsValues[item[s].Id][w].BuyPrice)
			//		//profit := float64(item[s].Buys.UnitPrice)*0.85 - float64(sigilsValues[item[s].Id][w].BuyPrice)
			//		sigilsValues[item[s].Id][w].Profit = profit
			//		if profit > 0{
			//			fmt.Println(sigilsValues[item[s].Id][w].Name + " is viable to extract !")
			//		}
			//	}
			//}
			profitSigils := ProcessUpgradeComponent(minProfit,sigilsValues, conf, ecto)
			fmt.Println("PROCESSING RUNES...")
			profitRunes := ProcessUpgradeComponent(minProfit,runesValues, conf, ecto)
			fmt.Printf("Profit profit for once of every item : %d\n", int(profitRunes+profitSigils))
			//fmt.Println(sigilsValues)
			//fmt.Println(len(runesValues))
		}
	},
}

func init() {
	rootCmd.AddCommand(extractCmd)
	extractCmd.Flags().BoolP("runes", "r", false, "do return runes")
	extractCmd.Flags().BoolP("sigils", "s", false, "do return sigils")
	extractCmd.Flags().Int8("min", 0, "min profit")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// extractCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// extractCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func ProcessUpgradeComponent(minProfit string,upgradeComponent map[int][]ItemExtract, conf Config, ecto ItemPrice) float64{
	ids := getUpgradesComponent(upgradeComponent)
	totProfit := 0.0
	var item []ItemPrice
	url := fmt.Sprintf("https://api.guildwars2.com/v2/commerce/prices?ids=%s&lang=%s", ids, conf.Lang)
	errJson := getJson(url,&item)
	if errJson != nil {
		fmt.Println(errJson)
	}
	for s:=0; s<len(item); s++ {
		for w:=0; w<len(upgradeComponent[item[s].Id]); w++{
			profit := float64(item[s].Buys.UnitPrice+ecto.Sells.UnitPrice)*0.899
			profit = profit*0.85
			profit -= float64(upgradeComponent[item[s].Id][w].BuyPrice)
			//profit := float64(item[s].Buys.UnitPrice)*0.85 - float64(sigilsValues[item[s].Id][w].BuyPrice)
			upgradeComponent[item[s].Id][w].Profit = profit
			minProfitFloat, errFloat := strconv.ParseFloat(minProfit,64)
			if errFloat != nil {
				fmt.Println(errFloat)
			}
			if profit > minProfitFloat{
				totProfit += profit
				fmt.Printf("%s is viable to extract for %d copper profit\n",upgradeComponent[item[s].Id][w].Name, int(profit))
			}
		}
	}
	return totProfit
}

func DoSetItemFiles(conf Config){
	var upgradeComponent []ItemExtract
	var armor []ItemExtract
	var weapon []ItemExtract
	sigilInWeapon := make(map[int][]ItemExtract)
	runeInArmor := make(map[int][]ItemExtract)
	errJson := getJson("https://api.silveress.ie/gw2/v1/items/json?fields=id&filter=type:UpgradeComponent", &upgradeComponent)
	if errJson != nil {
		fmt.Println("upgrade")
		fmt.Println(errJson)
	}
	//fmt.Println("gonna fetch armors")
	errJsonarmor := getJson("https://api.silveress.ie/gw2/v1/items/json?fields=id,name,rarity,upgrade1,upgrade2,sell_velocity,sell_price,buy_price&filter=type:Armor,marketable:TRUE", &armor)
	if errJsonarmor != nil {
		fmt.Println("armor")
		fmt.Println(errJsonarmor)
	}
	//fmt.Println("gonna fetch weapons")
	errJsonweapon := getJson("https://api.silveress.ie/gw2/v1/items/json?fields=id,name,rarity,upgrade1,upgrade2,sell_velocity,sell_price,buy_price&filter=type:Weapon,marketable:TRUE", &weapon)
	if errJsonweapon != nil {
		fmt.Println("weapons")
		fmt.Println(errJsonweapon)
	}

	//ARMOR TIME !!!
	//fmt.Printf("len armor : %d",len(armor))
	for s:=0; s<len(armor); s++  {
		if armor[s].Upgrade1 != 0 {
			_, ok := runeInArmor[armor[s].Upgrade1]
			if !ok {
				mm := []ItemExtract{armor[s]}
				runeInArmor[armor[s].Upgrade1] = mm
			}  else {
				runeInArmor[armor[s].Upgrade1] =  append(runeInArmor[armor[s].Upgrade1],armor[s])
			}
		}
		if armor[s].Upgrade2 != 0 {
			_, ok := runeInArmor[armor[s].Upgrade2]
			if !ok {
				mm := []ItemExtract{armor[s]}
				runeInArmor[armor[s].Upgrade2] = mm
			}  else {
				runeInArmor[armor[s].Upgrade2] =  append(runeInArmor[armor[s].Upgrade2],armor[s])
			}
		}
	}
	confJson,errJson := json.Marshal(runeInArmor)
	if errJson != nil {
		panic(errJson)
	}
	//fmt.Println(conf)
	//fmt.Println(conf.RunesArmor)
	err := ioutil.WriteFile(conf.RunesArmor, confJson, 0666)
	if err != nil {
		panic(err)
	}
	//fmt.Println("Json created at "+ conf.RunesArmor)

	//WEAPON TIME !!!
	//fmt.Printf("len weapon : %d",len(weapon))
	for s:=0; s<len(weapon); s++  {
		if weapon[s].Upgrade1 != 0 {
			_, ok := sigilInWeapon[weapon[s].Upgrade1]
			if !ok {
				mm := []ItemExtract{weapon[s]}
				sigilInWeapon[weapon[s].Upgrade1] = mm
			}  else {
				sigilInWeapon[weapon[s].Upgrade1] =  append(sigilInWeapon[weapon[s].Upgrade1],weapon[s])
			}
		}
		if weapon[s].Upgrade2 != 0 {
			_, ok := sigilInWeapon[weapon[s].Upgrade2]
			if !ok {
				mm := []ItemExtract{weapon[s]}
				sigilInWeapon[weapon[s].Upgrade2] = mm
			}  else {
				sigilInWeapon[weapon[s].Upgrade2] =  append(sigilInWeapon[weapon[s].Upgrade2],weapon[s])
			}
		}
	}
	confJsonSigil,errJsonsigil := json.Marshal(runeInArmor)
	if errJsonsigil != nil {
		panic(errJsonsigil)
	}
	//os.OpenFile(conf.SigilWeapon, os.O_RDONLY|os.O_CREATE, 0666)
	errSigil := ioutil.WriteFile(conf.SigilWeapon, confJsonSigil, 0666)
	if errSigil != nil {
		panic(errSigil)
	}
	//fmt.Println("Json created at " + conf.SigilWeapon)
}