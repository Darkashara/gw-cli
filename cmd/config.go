// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"io/ioutil"
	"os"
	"os/user"
	"runtime"
)

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Config your local profile",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("config called")
		show := cmd.Flag("show").Value.String()
		if show == "true" {
			conf := getConfig()
			fmt.Println("Apikey : "+conf.Apikey)
			fmt.Println("Language : "+conf.Lang)
			fmt.Println("Account : ")
			fmt.Println(conf.Account.Format())
		}else{
			os.Setenv("HTTP_PROXY", "http://gateway.seb.zscaler.net:80")
			os.Setenv("HTTPS_PROXY", "http://gateway.seb.zscaler.net:80")
			itemFile := CreateDirIfNotExist("gw-cli")
			err := DownloadFile(itemFile,"http://api.gw2tp.com/1/bulk/items-names.json")
			if err != nil {
				panic(err)
			}
		}
	},
}

func init() {
	configCmd.Flags().BoolP("show", "s", false, "show config")
	rootCmd.AddCommand(configCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// configCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// configCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func CreateDirIfNotExist(dir string) string{
	var conf Config
	usr, err := user.Current()
	if err != nil {
		fmt.Println( err )
	}
	directory := usr.HomeDir +"\\"+dir
	if runtime.GOOS == "windows" {
		directory = usr.HomeDir +"\\"+dir
	}else{
		directory = "/tmp/"+dir
	}
	if _, err := os.Stat(directory); os.IsNotExist(err) {
		err = os.MkdirAll(directory, 0755)
		fmt.Println("created")
		if err != nil {
			panic(err)
		}
	}
	fmt.Println("Please, provide a full accesss ApiKey : ")
	var responses string
	fmt.Scanln(&responses)
	conf.Apikey = responses
	var token Token
	getJson("https://api.guildwars2.com/v2/tokeninfo?access_token="+conf.Apikey,&token)
	fmt.Println("You'll get the following permissions :")
	if len(token.Permissions) == 0 {
		fmt.Println("You'll get no permissions")
	}
	if Contains(token.Permissions, "account") == false{
		fmt.Println("No Account permission found, CLI could crash")
	}
	for i:=0;i<len(token.Permissions) ;i++  {
		fmt.Println(token.Permissions[i])
	}
	fmt.Println("Please, choose your language between en or fr ")
	var lang string
	fmt.Scanln(&lang)
	expected := []string{"en","fr"}

	for Contains(expected, lang) == false {
		fmt.Println("Please, choose your language between en or fr ")
		fmt.Scanln(&lang)
	}
	conf.Lang = lang
	var accountInfo Account
	errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/account?access_token=%s",conf.Apikey), &accountInfo)
	if errJson != nil{
		fmt.Println(errJson)
	}
	conf.Account = accountInfo
	conf.Items = directory+"/items.json"
	conf.SigilWeapon = directory+"/sigilWeapon.json"
	conf.RunesArmor = directory+"/runeArmor.json"

	filePath := directory+"/conf.json"
	confJson,errJson := json.Marshal(conf)
	if errJson != nil {
		panic(errJson)
	}
	err = ioutil.WriteFile(filePath, confJson, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("your config files has been created at : "+ filePath)
	return conf.Items
}
