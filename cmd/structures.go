package cmd

import (
	"database/sql"
	"fmt"
	"time"
)

type Item struct {
	Id int `json:"id"`
	Name string `json:"name"`
	Description string `json:"description"`
	Type string `json:"container"`
	Level int `json:"level"`
}

type DataWarItem struct {
	Id int `json:"id"`
	BuyPrice int `json:"buy_price"`
	SellPrice int `json:"sell_price"`
	Name string `json:"name"`
	Profit int `json:"profit"`
}

type ItemExtract struct {
	Id int `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
	Rarity string `json:"rarity"`
	Upgrade1 int `json:"upgrade1"`
	Upgrade2 int `json:"upgrade2"`
	BuyPrice int `json:"buy_price"`
	SellPrice int `json:"sell_price"`
	SellVelocity int `json:"sell_velocity"`
	Profit float64
}

type silveressItem struct {
	Id int `json:"id"`
	Name int `json:"name"`
	LastUpdate time.Time `json:"lastUpdate"`
	BuyPrice int `json:"buy_price"`
	BuyQuantity int `json:"buy_quantity"`
	SellPrice int `json:"sell_price"`
	SellQuantity int `json:"sell_quantity"`
}

type SubItem struct {
	ItemID int `json:"item_id"`
	Count int `json:"count"`
}
/*
type Recipe struct {
	ID int `json:"id"`
	Type string `json:"type"`
	OutputItemID int `json:"output_item_id"`
	OutputItemCount int `json:"output_item_count"`
	MinRating int `json:"min_rating"`
	Disciplines []string `json:"disciplines"`
	Ingredients []SubItem `json:"ingredients"`
	Cost int
}*/

type Recipe struct {
	ID int `csv:"id"`
	Type string `csv:"type"`
	OutputItemID int `csv:"output_item_id"`
	OutputItemCount int `csv:"output_item_count"`
	MinRating int `csv:"min_rating"`
	Disciplines []string `csv:"disciplines"`
	Item1ID int `csv:"item_1_id"`
	Item1Quantity int `csv:"item_1_quantity"`
	Item1ShouldBeCraft bool
	Item1CraftPrice int
	Item2ID int `csv:"item_2_id"`
	Item2Quantity int `csv:"item_2_quantity"`
	Item2ShouldBeCraft bool
	Item2CraftPrice int
	Item3ID int `csv:"item_3_id"`
	Item3Quantity int `csv:"item_3_quantity"`
	Item3ShouldBeCraft bool
	Item3CraftPrice int
	Item4ID int `csv:"item_4_id"`
	Item4Quantity int `csv:"item_4_quantity"`
	Item4ShouldBeCraft bool
	Item4CraftPrice int
	Item5ID int `csv:"item_5_id"`
	Item5Quantity int `csv:"item_5_quantity"`
	Item5ShouldBeCraft bool
	Item5CraftPrice int
	Cost int
}

type RecipeJSON struct {
	ID int `json:"id"`
	Type string `json:"type"`
	OutputItemID int `json:"output_item_id"`
	OutputItemCount int `json:"output_item_count"`
	MinRating int `json:"min_rating"`
	Disciplines []string `json:"disciplines"`
	Ingredients []SubItem `json:"ingredients"`
	Cost int

}

/*
func (rd *RecipeJSON) Recipe() Recipe{
	return Recipe{
		ID: rd.ID,
		Type: rd.Type,
		OutputItemCount: rd.OutputItemCount,
		OutputItemID: rd.OutputItemID,
		MinRating: rd.MinRating,
		Disciplines: rd.Disciplines,
		Ingredients: rd.Ingredients,
		Cost: rd.Cost,
	}
}

func (r *Recipe) UnmarshalJSON(b []byte) error{
	var rd RecipeJSON
	if err := json.Unmarshal(b,&rd); err != nil {
		return err
	}
	var ingredientIndex = map[int]int{}
	var idToGet = []int{}
	for i:=0; i<len(rd.Ingredients); i++ {
		ingredientIndex[rd.Ingredients[i].ItemID] = rd.Ingredients[i].Count
		idToGet = append(idToGet, rd.Ingredients[i].ItemID)
	}
	itemURL := fmt.Sprintf("https://api.guildwars2.com/v2/commerce/prices?ids=%s", arrayToString(idToGet,","))
	var items []ItemPrice
	resp, err := http.Get(itemURL)
	if err != nil{
		fmt.Println(err)
	}
	byteBody,errRead :=ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if errRead != nil {
		fmt.Println(errRead)
	}
	json.Unmarshal(byteBody,&items)

	for j:=0; j<len(items); j++{
		rd.Cost += items[j].Buys.UnitPrice * ingredientIndex[items[j].Id]
	}

	*r = rd.Recipe()
	return nil
}*/


type ItemPrice struct {
	Id int `json:"id"`
	Whitelisted bool `json:"whitelisted"`
	Buys Price `json:"buys"`
	Sells Price `json:"sells"`
}

type Price struct {
	Quantity int `json:"quantity"`
	UnitPrice int `json:"unit_price"`
}

type Transaction struct {
	Id int64 `json:"id"`
	Item_id int `json:"item_id"`
	Price int `json:"price"`
	Quantity int `json:"quantity"`
	Is_filled bool
	//Created time.Time `json:"created"`
}

func (t *Transaction) Update(db *sql.DB) error{
	fmt.Println("updating Transaction %d",t.Id)
	//insertRequest:= fmt.Sprintf("update investments set quantity = %d, is_filled = %t where id = %d;",t.Quantity,t.Is_filled, t.Id)
	insertRequest := fmt.Sprintf("update config set last_timestamp = CURRENT_TIMESTAMP where id= 1")
	fmt.Println(insertRequest)
	_,err := db.Exec(insertRequest)
	if err != nil{
		return err
	}

	return nil
}

type Export struct{
	ID int `csv:"ID"`
	Name string `csv:"Name"`
	Unbound int `csv:"Unbound"`
	Soulbound int `csv:"Soulbound"`
	AccountBound int `csv:"AccountBound"`
	Total int `csv:"Total"`
	Account string `csv:"Account"`
	Location string `csv:"Location"`
	Sublocation string `csv:"Sublocation"`
}

type Raids struct {
	Done []string
}

type Pvp struct {
	PvpRank int `json:"pvp_rank"`
	PvpRankPoints int `json:"pvp_rank_points"`
	PvpRankRollovers int `json:"pvp_rank_rollovers"`
	Aggregate GameStats `json:"aggregate"`
	Professions GameProfessions `json:"professions"`

}

type PvPLadders struct {
	Ranked GameStats `json:"ranked"`
	SoloArenaRated GameStats `json:"soloarenarated"`
	TeamArenaRated GameStats `json:"teamarenarated"`
	Unraked GameStats `json:"unranked"`
}

type GameProfessions struct {
	Elementalist GameStats `json:"elementalist"`
	Engineer GameStats `json:"engineer"`
	Guardian GameStats `json:"guardian"`
	Mesmer GameStats `json:"mesmer"`
	Necromancer GameStats `json:"necromancer"`
	Ranger GameStats `json:"ranger"`
	Revenant GameStats `json:"revenant"`
	Thief GameStats `json:"thief"`
	Warrior GameStats `json:"warrior"`
}

type GameStats struct {
	Wins int `json:"wins"`
	Losses int `json:"losses"`
	Desertions int `json:"desertions"`
	Byes int `json:"byes"`
	Forfeit int `json:"forfeit"`
}

type Game struct {
	Id string `json:"id"`
	MapId int `json:"map_id""`
	//Started time.Time `json:"started"`
	//Ended time.Time `json:"ended"`
	Result string `json:"result"`
	Team string `json:"team"`
	Profession string `json:"profession"`
	RatingType string `json:"rating_type"`
	RatingChange int `json:"rating_change"`
	Season string `json:"season"`
	Scores Score `json:"scores"`
}

type Score struct {
	Red int `json:"red"`
	Blue int `json:"blue"`
}

type Season struct {
	Current Standing `json:"current"`
	Best Standing `json:"best"`
}

type Standing struct {
	TotalPoints int `json:"total_points"`
	Division int `json:"division"`
	Tier int `json:"tier"`
	Points int `json:"points"`
	Repeats int `json:"repeats"`
}

type Account struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Age int64 `json:"age"`
	World int `json:"world"`
	Guilds []string `json:"guilds"`
	GuildLeader []string `json:"guild_leader"`
	//Created time.Time `json:"create"`
	Access []string `json:"access"`
	Commander bool `json:"commander"`
	FractalLevel int `json:"fractal_level"`
	DailyAp int `json:"daily_ap"`
	MonthlyAp int `json:"monthly_ap"`
	WvwRank int `json:"wvw_rank"`
}

type Material struct {
	Id int `json:"id"`
	Category int `json:"category"`
	Binding string `json:"binding"`
	Count int `json:"count"`
}

type Config struct {
	Apikey string `json:"Apikey"`
	Lang string `json:"Lang"`
	Items string `json:"items"`
	RunesArmor string `json:"runesArmor"`
	SigilWeapon string `json:"sigilWeapon"`
	Account Account `json:"Account"`
	Guilds []string `json:"guilds"`
}
type ItemName struct {
	Items []ItemNameIn `json:"items"`
}
type ItemNameIn struct {
	Id int `json:"id"`
	Name string `json:"name"`
}

type ItemDB struct {
	Items []ArrayFlex `json:"items"`
}
type ArrayFlex []FlexItem
type FlexItem string

type Token struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Permissions []string `json:"permissions"`
}

type Listings struct {
	Id int `json:"id"`
	Buys []listing `json:"buys"`
	Sells []listing `json:"sells"`
}

type listing struct {
	Listings int `json:"listings"`
	UnitPrice int `json:"unit_price"`
	Quantity int `json:"quantity"`
}

type Bank struct {
	Id int  `json:"id"`
	Count int `json:"count"`
	Skin int `json:"skin"`
	Upgrades []int `json:"upgrades"`
	Dyes []int `json:"dyes"`
	Binding string `json:"binding"`
	BoundTo string `json:"cound_to"`
}

type GuildLogs struct {
	Logs []GuildLog
}

type GuildLog struct {
	Id int `json:"id"`
	Time time.Time `json:"time"`
	Type string `json:"type"`
	User string `json:"user"`
	ItemId int `json:"item_id"`
	Count int `json:"count"`
	Motd string `json:"motd"`
	InvitedBy string `json:"invited_by"`
	Operation string `json:"operation"`
	Coins int `json:"coins"`
	UpgradeId int `json:"upgrade_id"`
	Action string `json:"action"`
}

type GuildMembers struct {
	Name string `json:"name"`
	Rank string `json:"rank"`
	Joined time.Time `json:"joined"`
}

type GuildRanks struct {
	ID string `json:"id"`
	Order int `json:"order"`
	Permissions []string `json:"permissions"`
	Icon string `json:'icon'`
}

type GuildStash struct {
	UpgradeId int  `json:"upgrade_id"`
	Size int `json:"size"`
	Coins int `json:"coins"`
	Note string `json:"note"`
	Inventory []GuildObject `json:"inventory"`
}

type GuildObject struct {
	ID int `json:"id"`
	Count int `json:"count"`
}

type GuildTreasury struct {
	ItemId int `json:"item_id"`
	Count int `json:"count"`
	NeededBy []GuildNeed
}

type GuildNeed struct {
	UpgradeId int `json:"upgrade_id"`
	Count int `json:"count"`
}

type GuildTeams struct {

}

type GuildUpgrades struct {

}