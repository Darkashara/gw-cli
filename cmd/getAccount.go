// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"strconv"
)

// getAccountCmd represents the getAccount command
var getAccountCmd = &cobra.Command{
	Use:   "account",
	Short: "Get informations about your account",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		//apikey := cmd.Flag("apikey").Value.String()
		id := cmd.Flag("id").Value.String()
		conf := getConfig()
		expected := []string{"infos", "materials","inventory","wallet"}
		if Contains(expected, args[0]) {
			if args[0] == "infos" {
				var accountInfo Account
				errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/account?access_token=%s",conf.Apikey), &accountInfo)
				if errJson != nil{
					fmt.Println(errJson)
				}
				//fmt.Println(accountInfo.Format())
				fmt.Println(accountInfo)
			}else if args[0] == "materials" {
				if id != ""{
					var materials []Material
					errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/account/materials?access_token=%s",conf.Apikey), &materials)
					if errJson != nil{
						fmt.Println(errJson)
					}
					idInt,_ := strconv.Atoi(id) //Must Handle error TODO
					for i:=0; i< len(materials) ; i++  {
						if idInt == materials[i].Id {
							fmt.Println(materials[i].Format())
							break
						}
					}
				}else{
					// WIP Printing all materials
					fmt.Println("all your materials")
					var materials []Material
					//var ids []int
					errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/account/materials?access_token=%s",conf.Apikey), &materials)
					if errJson != nil{
						fmt.Println(errJson)
					}
					var url string
					var counter int
					for i:=0; i< len(materials); i++{
						url += strconv.Itoa(materials[i].Id) + ","
						counter += 1
						if counter == 199 || i == len(materials)-1{
							var name []Item
							fmt.Println("before the items")
							errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items?ids=%s"+"?lang=fr", url), &name)
							if errJson != nil{
								fmt.Println(errJson)
							}
							for n:=0; n<len(name) ; n++  {
								for j:=0; j<len(materials) ; j++  {
									if materials[j].Id == name[n].Id {
										fmt.Println(name[n].Name, " : ", materials[j].Count)
										break
									}
								}

							}
							counter = 0
							url = ""
						}
					}
				}
			}
		}else{
			fmt.Printf("Please use one of the following arg : infos, materials, inventory, wallet")
			os.Exit(0)
		}

	},
}

func init() {
	getAccountCmd.Flags().String("id", "", "human version")
	//getAccountCmd.PersistentFlags().String("apikey", "", "Provide apikey with PvP rights")
	rootCmd.AddCommand(getAccountCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getAccountCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getAccountCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
