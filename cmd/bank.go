// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

// bankCmd represents the bank command
var bankCmd = &cobra.Command{
	Use:   "bank",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		id := cmd.Flag("id").Value.String()
		conf := getConfig()
		if id != ""{
			var banks []Bank
			errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/account/bank?access_token=%s",conf.Apikey), &banks)
			if errJson != nil{
				fmt.Println(errJson)
			}
			idInt,errConv := strconv.Atoi(id) //Must Handle error TODO
			if errConv != nil{
				fmt.Println("ID must be a number !")
				os.Exit(0)
			}
			for i:=0; i< len(banks) ; i++  {
				if idInt == banks[i].Id {
					fmt.Println(banks[i].Format())
					break
				}
			}
		}else{
			// WIP Printing all materials
			fmt.Println("all your bank item : ")
			var banks []Bank
			//var ids []int
			errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/account/bank?access_token=%s",conf.Apikey), &banks)
			if errJson != nil{
				fmt.Println(errJson)
			}
			var url string
			var counter int
			for i:=0; i< len(banks); i++{
				url += strconv.Itoa(banks[i].Id) + ","
				counter += 1
				if counter == 199 || i == len(banks)-1{
					var name []Item
					errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items?ids=%s"+"?lang=fr", url), &name)
					if errJson != nil{
						fmt.Println(errJson)
					}
					for n:=0; n<len(name) ; n++  {
						for j:=0; j<len(banks) ; j++  {
							if banks[j].Id == name[n].Id {
								fmt.Println(name[n].Name, " : ", banks[j].Count)
								break
							}
						}

					}
					counter = 0
					url = ""
				}
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(bankCmd)
	bankCmd.Flags().String("id", "", "human version")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// bankCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// bankCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
