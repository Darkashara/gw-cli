// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// getPriceCmd represents the getPrice command
var getPriceCmd = &cobra.Command{
	Use:   "price",
	Short: "get price prices and profit of an item id",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		conf := getConfig()
		id := cmd.Flag("id").Value.String()
		if id != "" {
			var item []ItemPrice
			url := fmt.Sprintf("https://api.guildwars2.com/v2/commerce/prices?ids=%s&lang=%s", id, conf.Lang)
			errJson := getJson(url,&item)
			if errJson != nil {
				fmt.Println(errJson)
			}
			fmt.Println(item[0].Format())
		}else{
			fmt.Println("flag --id can't be empty.")
		}

	},
}

func init() {
	rootCmd.AddCommand(getPriceCmd)
	getPriceCmd.PersistentFlags().String("id", "", "A help for id")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getPriceCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getPriceCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
