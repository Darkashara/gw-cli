// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	//"os"

	"github.com/spf13/cobra"
)

// getNameCmd represents the getName command
var getNameCmd = &cobra.Command{
	Use:   "name",
	Short: "Get informations about an item id",
	Long: `This commande returns information about an Item from it's ID or it's name
			For instance search by name is only available in english !`,
	//Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		//fmt.Println("getName called")
		conf := getConfig()
		id := cmd.Flag("id").Value.String()
		name := cmd.Flag("name").Value.String()
		if name != "" || id != ""{
			if name != "" {
				filename := conf.Items
				fmt.Println(filename)
				plan, errRead := ioutil.ReadFile(filename)
				if errRead != nil{
					fmt.Println(errRead)
					fmt.Print("Please set your profile with : gw-cli config")
					os.Exit(0)
				}
				var data ItemName
				err := json.Unmarshal([]byte(plan), &data)
				if err != nil{
					fmt.Println(err)
					fmt.Print("Please set your profile with : gw-cli config")
					os.Exit(0)
				}
				for i:=0; i<len(data.Items); i++ {
					if data.Items[i].Name  == name {
						fmt.Println("Item Name : ",name," ID : ", data.Items[i].Id)
					}
				}
			} else {
				var item Item
				url := fmt.Sprintf("https://api.guildwars2.com/v2/items/%s?lang=%s", id, conf.Lang)
				errJson := getJson(url,&item)
				if errJson != nil {
					fmt.Println(errJson)
				}
				if item.Id == 0 {
					fmt.Println("This item id does not exist")
				} else {
					fmt.Println(item)
				}
			}
		} else {
			fmt.Println("Please provide a name with the flag --name or an id with the flag --id !")
		}

	},
}

func init() {
	rootCmd.AddCommand(getNameCmd)
	getNameCmd.Flags().String("id", "", "Get Item Name from ID")
	getNameCmd.Flags().String("name", "", "get the item id from the name")
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getNameCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getNameCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}