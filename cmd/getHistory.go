// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

// getHistoryCmd represents the getHistory command
var getHistoryCmd = &cobra.Command{
	Use:   "getHistory",
	Short: "get your current or history transactions",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		human := cmd.Flag("human").Value.String()
		expected := []string{"history", "current"}
		expected_args := []string{"sells", "buys"}
		if len(args) < 2 {
			fmt.Printf("Please use : gw-cli getHistory [current/history] [sells/buys]")
			os.Exit(0)
		}else if Contains(expected,args[0]) && Contains(expected_args, args[1]){
			if human == "true" {
				fmt.Print("human is true")
			}else{
				fmt.Println(human)
			}
			transac := getTradingPost(args[0],args[1])
			toDisplay := make(map[int]map[int]int)
			for i:=0;i < len(transac) ; i++ {
				if _, ok := toDisplay[transac[i].Item_id]; ok {
					if _, ok := toDisplay[transac[i].Item_id][transac[i].Price]; ok {
						//fmt.Println(toDisplay[transac[i].Item_id][transac[i].Price])
						toDisplay[transac[i].Item_id][transac[i].Price] += transac[i].Quantity
					}else {
						toDisplay[transac[i].Item_id][transac[i].Price] = transac[i].Quantity
					}
				}else{
					if toDisplay[transac[i].Item_id] == nil {
						toDisplay[transac[i].Item_id] = make(map[int]int)
						toDisplay[transac[i].Item_id][transac[i].Price] = transac[i].Quantity
					}else{
						toDisplay[transac[i].Item_id][transac[i].Price] = transac[i].Quantity
					}
				}
			}
			FormatHistory(toDisplay)
		}else{
			fmt.Printf("Please use : gw-cli getHistory [current/history] [sells/buys]")
		}
	},
}

func init() {

	//getHistoryCmd.PersistentFlags().String("apikey", "", "apikey with BLTC rights")
	getHistoryCmd.Flags().BoolP("human", "w", false, "human version")
	getHistoryCmd.SetHelpCommand(&cobra.Command{
		Use:    "Please use : gw-cli getHistory [current/history] [sells/buys]",
		Hidden: false,
	})
	rootCmd.AddCommand(getHistoryCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getHistoryCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getHistoryCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
