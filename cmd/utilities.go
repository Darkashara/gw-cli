package cmd

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gocarina/gocsv"
	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"strings"
)
const SilverURL = "https://api.guildwars2.com/"
const DataWarsURL = "https://api.datawars2.ie/gw2/v2/"
var LogMapping = map[string]log.Level{
	"debug":log.DebugLevel,
	"info":log.InfoLevel,
	"warn": log.WarnLevel,
	"error": log.ErrorLevel,
	}

func getJson(url string, target interface{}) error {
	r, err := http.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	//fmt.Println(r.Body)
	return json.NewDecoder(r.Body).Decode(target)
}

func arrayToString(a []int, delim string) string {
	return strings.Trim(strings.Replace(fmt.Sprint(a), " ", delim, -1), "[]")
}

func readCsvFile(filePath string) []Export{
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read input file "+filePath,err)
	}
	defer f.Close()
	 exports := []Export{}

	 if err := gocsv.UnmarshalFile(f,&exports); err != nil{
		 log.Fatal("Unable to unMarshal csv to struct for file "+ filePath,err)
	 }

	return exports
}

func getCsv(url string, target interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	err = gocsv.Unmarshal(resp.Body, target)
	if err != nil {
		return err
	}

	return nil
}

func dumpToCSV(items interface{}) error{
	fmt.Println("dumpToCSV calles")
	 file, err := os.Create("/tmp/my_dump.csv")
	 if err != nil{
		 return err
	 }
	 if err := gocsv.MarshalFile(items,file); err != nil {fmt.Println(err)}
	return nil
}

func dumpToInvestTable(items []Transaction) error{
	db, _ := sql.Open("sqlite3", "./gw2.db")
	insertRequest:= "INSERT INTO investments(id,item_id, price, quantity) VALUES (?,?,?,?)"
	statement,insertErr := db.Prepare(insertRequest)
	if insertErr != nil{
		return insertErr
	}
	for i:=0; i<len(items); i++{
		statement.Exec(items[i].Id,items[i].Item_id,items[i].Price,items[i].Quantity)
		//fmt.Println("item %s has been inserted", items[i].Id)
	}
	return nil
}

func dumptToExportTable(exports []Export, exportType string) error{
	db, _ := sql.Open("sqlite3", "./gw2.db")
	uuid := uuid.New()
	insertRequest:= "INSERT INTO exports(export_id, item_id, count, export_type) VALUES (?,?,?,?)"
	statement,insertErr := db.Prepare(insertRequest)
	if insertErr != nil{
		return insertErr
	}
	for i:=0; i<len(exports); i++{
		statement.Exec(uuid,exports[i].ID,exports[i].Total,exportType)
		//fmt.Println("item %s has been inserted", items[i].Id)
	}
	return nil
}


func getTradingPost(transaction string,transationType string)([]Transaction){
	conf := getConfig()
	var transac []Transaction
	url := fmt.Sprintf("https://api.guildwars2.com/v2/commerce/transactions/%s/%s?access_token=%s", transaction, transationType,conf.Apikey)
	errJson := getJson(url,&transac)
	if errJson != nil {
		fmt.Println(errJson)
	}
	return transac
}

func getConfig() (Config){
	var data Config

	filename := "/tmp/gw-cli/conf.json"
	plan, errRead := ioutil.ReadFile(filename)
	if errRead != nil{
		fmt.Print("Please set your profile with : gw-cli config")
		os.Exit(0)
	}
	err := json.Unmarshal(plan, &data)
	if err != nil{
		fmt.Print("Please set your profile with : gw-cli config")
		os.Exit(0)
	}
	return data
}

func setProfit(items []ItemExtract){
	for i:=0; i<len(items); i++  {
		items[i].Profit = 10.0
	}
}

func getItemExtract(conf Config) (map[int][]ItemExtract, map[int][]ItemExtract){
	runes := make(map[int][]ItemExtract)
	sigils := make(map[int][]ItemExtract)

	sigilFile, errRead := ioutil.ReadFile(conf.SigilWeapon)
	runeFile, errRead := ioutil.ReadFile(conf.RunesArmor)
	if errRead != nil{
		fmt.Print("Could not load Items Please try : gw-cli config")
		os.Exit(0)
	}
	err := json.Unmarshal(sigilFile, &sigils)
	err = json.Unmarshal(runeFile, &runes)
	if err != nil{
		fmt.Print("Please set your profile with : gw-cli config")
		os.Exit(0)
	}
	return sigils, runes
}

func getUpgradesComponent(items map[int][]ItemExtract) string{
	//keys := make([]int, 0, len(items))
	keys := ""
	for k := range items {
		keys += strconv.Itoa(k)+","
	}
	return keys
}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func (item ItemPrice) Format() string{
	var name Item
	fmt.Println("https://api.guildwars2.com/v2/items/%s"+"?lang=fr")
	errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items/%s"+"?lang=fr", strconv.Itoa(item.Id)), &name)
	if errJson != nil{
		fmt.Println(errJson)
	}
	profit := float64(item.Buys.UnitPrice)*0.85 - float64(item.Sells.UnitPrice)
	return fmt.Sprintf("%s | Buy : %s | Sell : %s | Profit : %f",name.Name ,strconv.Itoa(item.Buys.UnitPrice),strconv.Itoa(item.Sells.UnitPrice), profit)
}

func (log GuildLog) Format() string{
		switch log.Type {
		case "motd":
			return fmt.Sprintf("%s | Buy : %s | Sell : %s | Profit : %f")
		case "stash":
			var name Item
			errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items/%s"+"?lang=en", strconv.Itoa(log.ItemId)), &name)
			if errJson != nil{
				fmt.Println(errJson)
			}
			return fmt.Sprintf("%s %s %d %s in %s ",log.User, log.Operation, log.Count, name.Name, log.Type)
		case "treasury":
			var name Item
			errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items/%s"+"?lang=fr", strconv.Itoa(log.ItemId)), &name)
			if errJson != nil{
				fmt.Println(errJson)
			}
			return fmt.Sprintf("%s %s %d %s in %s ",log.User, log.Operation, log.Count, name.Name, log.Type)

		}

	return fmt.Sprintf(log.Type)

}

func FormatHistory(hist map[int]map[int]int) {
	var name Item
	for itemId, item := range hist {
		errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items/%s"+"?lang=fr", strconv.Itoa(itemId)), &name)
		if errJson != nil{
			fmt.Println(errJson)
		}
		for price, quantity := range item {
			fmt.Printf("%s | Quantity : %s | Price : %s \n",name.Name ,strconv.Itoa(quantity),strconv.Itoa(price))

		}
	}

}

//func (name Item) Format() string{
//		return fmt.Sprintf("%s | Quantity : %s | Price : %s",name.Name ,name.Type,strconv.Itoa(transac.Price))
//}

func (raids Raids) Format() string{
	var formatedRaids string
	if len(raids.Done) == 0{
		formatedRaids = "0 Raid done this week"
	}else{
		formatedRaids = "Raids dones this week : "
		for i:=0; i<len(raids.Done) ; i++ {
			formatedRaids+=raids.Done[i]+", "
		}
	}
	return formatedRaids
}

func (game GameStats) Format() string{
	return fmt.Sprintf("Wins : %d | Losses : %d | Desertions : %d | Byes : %d | Forfeit : %d", game.Wins, game.Losses, game.Desertions, game.Byes, game.Forfeit)
}

func (game Game) Format() string{
	return fmt.Sprintf("%s | Red : %d | Blue : %d | Profession : %s", game.Result, game.Scores.Red, game.Scores.Blue, game.Profession)
}
func (season Season) Format() string{
	return fmt.Sprintf("Division: %d/%d | Tier : %d/%d | Points : %d/%d", season.Current.Division, season.Best.Division,season.Current.Tier,season.Best.Tier,season.Current.Points, season.Best.Points)
}
func (acc Account) Format() string{
	//return fmt.Sprintf("Division: %d/%d | Tier : %d/%d | Points : %d/%d", season.Current.Division, season.Best.Division,season.Current.Tier,season.Best.Tier,season.Current.Points, season.Best.Points)
	var guilds string
	for i:= 0;i<len(acc.Guilds) ;i++  {
		fmt.Println(acc.Guilds[i])
		guilds += acc.Guilds[i]+" | "
	}
	var guildLeader string
	for i:= 0;i<len(acc.GuildLeader) ;i++  {
		guildLeader += acc.GuildLeader[i]+" | "
	}
	return fmt.Sprintf(`Account : 
	Name : %s 
	Guilds : %s
	Guild Leader : %s`, acc.Name, guilds, guildLeader)
	//return "toto"
}

func (mat Material) Format() string{
	//return fmt.Sprintf("Division: %d/%d | Tier : %d/%d | Points : %d/%d", season.Current.Division, season.Best.Division,season.Current.Tier,season.Best.Tier,season.Current.Points, season.Best.Points)
	var name Item
	errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items/%s"+"?lang=fr", strconv.Itoa(mat.Id)), &name)
	if errJson != nil{
		fmt.Println(errJson)
	}
	return fmt.Sprintf("Name : %s | Quantity : %d", name.Name, mat.Count)
}

func (bank Bank) Format() string{
	//return fmt.Sprintf("Division: %d/%d | Tier : %d/%d | Points : %d/%d", season.Current.Division, season.Best.Division,season.Current.Tier,season.Best.Tier,season.Current.Points, season.Best.Points)
	var name Item
	errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items/%s"+"?lang=fr", strconv.Itoa(bank.Id)), &name)
	if errJson != nil{
		fmt.Println(errJson)
	}
	return fmt.Sprintf("Name : %s | Quantity : %d", name.Name, bank.Count)
}

func (list listing) Format() string{
	return fmt.Sprintf("Price: %d | Quantity : %d | Number of Listings : %d", list.UnitPrice, list.Quantity, list.Listings)

	//return "toto"
}

func (member GuildMembers) Format() string{
	return fmt.Sprintf("%s is %s and member since %s", member.Name, member.Rank, member.Joined.Format("2006-01-02 15:04:05"))

	//return "toto"
}

func (member GuildRanks) Format() string{
	var perms string
	for i:=0; i<len(member.Permissions); i++ {
		perms += member.Permissions[i] +" | "
	}
	return fmt.Sprintf("%s rank got %s permissions", member.ID, perms)

	//return "toto"
}

func (treasury GuildTreasury) Format() string {
	var name Item
	errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items/%s"+"?lang=fr", strconv.Itoa(treasury.ItemId)), &name)
	if errJson != nil{
	fmt.Println(errJson)
	}
	return fmt.Sprintf("Name : %s | Quantity : %d", name.Name, treasury.Count)
}

func (guildItem GuildObject) Format() string{
	var name Item
		errJson := getJson(fmt.Sprintf("https://api.guildwars2.com/v2/items/%s"+"?lang=fr", strconv.Itoa(guildItem.ID)), &name)
		if errJson != nil{
			fmt.Println(errJson)
		}
		return fmt.Sprintf("Name : %s | Quantity : %d", name.Name, guildItem.Count)
}

func PackIds(ids []int) string{
		url := ""
		//myElem := ids[i:min((i + 200), len(ids))]
		for id := 0; id < len(ids); id++ {
			//r := reflect.ValueOf(myElem[id])
			//stats := r.FieldByName("Id").String()
			url += url + strconv.Itoa(ids[id]) + ","
		}
	return url
}

func removeMaterials(slice []Material, s int) []Material {
	return append(slice[:s], slice[s+1:]...)
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func (v GameProfessions)getField(field string) GameStats{
	var stats GameStats
	r := reflect.ValueOf(v)
	stats = r.FieldByName(field).Interface().(GameStats)
	return stats
}

func DownloadFile(filepath string, url string) error {

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func (fi *FlexItem) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	*fi = FlexItem(s)
	return nil
}

func (n *ItemNameIn) UnmarshalJSON(buf []byte) error {
	tmp := []interface{}{&n.Id, &n.Name}
	wantLen := len(tmp)
	if err := json.Unmarshal(buf, &tmp); err != nil {
		return err
	}
	if g, e := len(tmp), wantLen; g != e {
		return fmt.Errorf("wrong number of fields in ItemNameIn: %d != %d", g, e)
	}
	return nil
}