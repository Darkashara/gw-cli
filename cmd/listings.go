// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

// listingsCmd represents the listings command
var listingsCmd = &cobra.Command{
	Use:   "listings",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//fmt.Println("listings called")
		id := cmd.Flag("id").Value.String()
		wall := cmd.Flag("wall").Value.String()
		max := cmd.Flag("max").Value.String()
		var item Listings
		url := fmt.Sprintf("https://api.guildwars2.com/v2/commerce/listings/%s", id)
		errJson := getJson(url,&item)
		if errJson != nil {
			fmt.Println(errJson)
		}
		if wall != "w" {
			fmt.Println("wall : ",wall)
			intWall,errConv := strconv.Atoi(wall)
			if errConv != nil {
				fmt.Println(errConv)
			}
			for i:=0; i<len(item.Sells) ; i++  {
				if item.Sells[i].Quantity >= intWall{
					fmt.Println("Wall found at price : ",item.Sells[i].UnitPrice)
					break
				}
			}
		}else {
			var maxPrintSells int
			var maxPrintBuys int
			if max != "m" {
				maxPrint2,errConv := strconv.Atoi(max)
				if errConv != nil {
					fmt.Println(errConv)
				}
				maxPrintSells = maxPrint2
				maxPrintBuys = maxPrint2
			}else {
				maxPrintSells = len(item.Sells)
				maxPrintBuys = len(item.Buys)
			}
			fmt.Println("Sells :")
			for i := 0; i<maxPrintSells; i++ {
				fmt.Println(item.Sells[i].Format())
			}
			fmt.Println("Buys : ")
			for i := 0; i<maxPrintBuys; i++ {
				fmt.Println(item.Buys[i].Format())
			}
		}
	},
}

func init() {
	listingsCmd.PersistentFlags().String("id", "", "A help for id")
	listingsCmd.Flags().String("wall", "w", "Get Wall")
	listingsCmd.Flags().String("max", "m", "Max return")
	rootCmd.AddCommand(listingsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// listingsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// listingsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
