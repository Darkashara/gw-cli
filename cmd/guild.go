// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"reflect"
	"strconv"
)

// guildCmd represents the guild command
var guildCmd = &cobra.Command{
	Use:   "guild",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		conf := getConfig()
		limit,_ := strconv.Atoi(cmd.Flag("limit").Value.String())
		base_URL := "https://api.guildwars2.com/v2/guild/"
		//https://api.guildwars2.com/v2/guild/4BBB52AA-D768-4FC6-8EDE-C299F2822F0F/members
		expected := []string{"leader", "member"}
		guild_auth := []string{"log","members","ranks","stash","treasury","teams","upgrades"}
		//guild := []string{"id","emblem","permissions","search","upgrades"}
		/*
			guild/:id: Returns core details about a given guild.
			emblem: Returns image resources needed to render guild emblems.
			guild/permissions: Returns information about guild rank permissions.
			guild/search: Returns information on guild ids to be used for other API queries.
			guild/upgrades: Returns information about guild upgrades and scribe decorations.
			Authenticate :
			guild/:id/log: Returns information about a guild's event log.
			guild/:id/members: Returns information about members of a guild.
			guild/:id/ranks: Returns information about the permission ranks of a guild.
			guild/:id/stash: Returns information about the contents of a guild's stash.
			guild/:id/treasury: Returns information about a guild's treasury contents.
			guild/:id/teams: Returns information about a guild's teams.
			guild/:id/upgrades: Returns information about a guild's upgrades*/

		if Contains(expected, args[0]) {
			if args[0] == "leader"{
				for i:=0; i<len(conf.Account.GuildLeader) ; i++  {
					fmt.Println("Guild ID : ",conf.Account.GuildLeader[i])

				}
				fmt.Println("Please copy/paste a Guild ID : ")
				var responses string
				fmt.Scanln(&responses)
				selec := "["
				for t:=0; t<len(guild_auth) ; t++  {
					selec += guild_auth[t]+"/"
				}
				select_last :=selec[:len(selec)-1]
				select_last += "]"
				fmt.Println("Please choose an option : ", select_last)
				var resp_option string
				fmt.Scanln(&resp_option)
				//var result interface{}
				if resp_option == "log" {
					result := []GuildLog{}
					errJson := getJson(fmt.Sprintf(base_URL+responses+"/"+resp_option+"?access_token=%s",conf.Apikey), &result)
					if errJson != nil{
						fmt.Println(errJson)
					}
					if len(result) < limit{
						limit = len(result)
					}
					for n:=0; n<limit ; n++  {
						fmt.Println(result[n].Format())
					}

					//TO.DO => Ajouter autres que withdraw / deposit

				} else if resp_option == "members" {
					result := []GuildMembers{}
					errJson := getJson(fmt.Sprintf(base_URL+responses+"/"+resp_option+"?access_token=%s",conf.Apikey), &result)
					if errJson != nil{
						fmt.Println(errJson)
					}
					if len(result) < limit{
						limit = len(result)
					}
					for n:=0; n<limit ; n++  {
						fmt.Println(result[n].Format())
					}
				} else if resp_option == "ranks" {
					result := []GuildRanks{}
					errJson := getJson(fmt.Sprintf(base_URL+responses+"/"+resp_option+"?access_token=%s",conf.Apikey), &result)
					if errJson != nil{
						fmt.Println(errJson)
					}
					if len(result) < limit{
						limit = len(result)
					}
					for n:=0; n<limit ; n++  {
						fmt.Println(result[n].Format())
					}
				} else if resp_option == "stash" {
					result := []GuildStash{}
					errJson := getJson(fmt.Sprintf(base_URL+responses+"/"+resp_option+"?access_token=%s",conf.Apikey), &result)
					if errJson != nil{
						fmt.Println(errJson)
					}
					fmt.Println("Coins : ",result[0].Coins)
					for n:=0; n<limit ; n++  {
						if result[0].Inventory[n].ID > 0{
							fmt.Println(result[0].Inventory[n].Format())
						}
					}
				} else if resp_option == "treasury" {
					result := []GuildTreasury{}
					errJson := getJson(fmt.Sprintf(base_URL+responses+"/"+resp_option+"?access_token=%s",conf.Apikey), &result)
					if errJson != nil{
						fmt.Println(errJson)
					}
					//fmt.Println("Coins : ",result[0].Coins)
					for n:=0; n<limit ; n++  {
						if result[n].ItemId > 0{
							fmt.Println(result[n].Format())
						}
					}
				} else if resp_option == "teams" {
					//result := GuildTeams{}
				} else if resp_option == "upgrades" {
					//result := GuildUpgrades{}
				}
			}else if args[0] == "member" {
				for i:=0; i<len(conf.Account.Guilds) ; i++  {
					fmt.Println("Guild ID : ",conf.Account.Guilds[i])
					fmt.Println("Please copy/paste a Guild ID : ")
					var responses string
					fmt.Scanln(&responses)
				}
			}
		}else {
			fmt.Printf("Please use : gw-cli guild [leader/member]")
			os.Exit(0)
		}
	},
}

func init() {
	rootCmd.AddCommand(guildCmd)
	guildCmd.Flags().IntP("limit", "l", 10, "max result")
	guildCmd.SetHelpCommand(&cobra.Command{
		Use:    "Please use : gw-cli guild [/]",
		Hidden: false,
	})
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// guildCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// guildCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func DoPrint(inter interface{}, base_URL string, responses string, conf Config, limit int){
	result := reflect.New(reflect.TypeOf(inter))
	errJson := getJson(fmt.Sprintf(base_URL+responses+"/"+"log"+"?access_token=%s",conf.Apikey), &result)
	if errJson != nil{
		fmt.Println(errJson)
	}
	fmt.Println()
	for l:=0; l<limit; l++ {
		fmt.Println("je suis là")
		toto := reflect.ValueOf(result)
		toto.Interface()
		fmt.Println(reflect.ValueOf(result).MethodByName("Format").Call([]reflect.Value{}))
	}
}
