A complete Command Line Interface for GuildWars2 API

Your GuildWars 2 Account as accessible as /bin/bash !

# Usage:

  gw-cli [command] [SubCommand] --flags

# Available Commands:

## config
   Config your local profile

    SubCommands : None

    Flags :

        --show

        --help

## getAccount
  Get informations about your account

    SubCommands : [infos/materials]

     Flags :

        --help

## getHistory
 Gget your current or history transactions

    SubCommands : [current/history] [sells/buys]

    Flags:

        --help

## getName
  Get informations about an item id

    SubCommands : None

    Flags : --id "item_id"

## getPrice
  Get price prices and profit of an item id

    SubCommands : Nona

    Flags : --is "item_id"

## getPvp
  Get your PvP stats, games or standings

    SubCommands : [stats/games/standings]

    Flags :

        --class "Guardian" (with capital letter)

        --id "game_id" (subcommand games without flag id provides all games id, with flag id, returns games informations)

        --help

## getRaids
  Get Raids you've done this week

    --help

## help
  Help about any command



# Flags:

    -h, --help            help for gw-cli



Use "gw-cli [command] --help" for more information about a command.

